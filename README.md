# Demo Animal Game Readme #

This is a prototype BackEnd API of a simple Animal Game.

* A User is created.
* A User can create a/many animals (dog/cat/rabbit).
* When an animal is created it starts with a neutral food and mood level.
* When you feed the animal, its food level increases, up to a maximum threshold.
* When you pet the animal, its mood level increases, up to a maximum threshold.
* Each animal has different BurnRates for their mood and food levels.
* An animal's mood and food level will decrease by 1 at each animals specific BurnRate, unless it has been fed/pet in within the time of the BurnRate.
* When an animal's Mood or Food level drops to 0, the animal is killed.

### Prerequisites ###

In order to run this solution out of the box, you will need to:

1. Have a sql server instance, I have specified (localdb)\v11.0 to keep things small and lightweight.
2. Have sqlcmd.exe installed and available in your Machine's Environment Variables (Comes with Sql Server installed). This is used by the Test project to connect to the Sql Instance and drop the Animal and User Test Databases prior to running the Tests.

### Installation Instructions ###

1. Load up the solution
2. Build the solution in "Debug" Configuration
3. Run the tests in the Integration.Test project. This will drop any existing DemoGame.Test.Animal and DemoGame.Test.User databases from our SQL instance (localdb)\v11.0

When the tests have passed you can build in Release to run up and use the system using the API.

Build the solution in "Release" Configuration.
    * This will perform transforms on the App.Config files for all the Ops projects to use a different Sql Database in the same instance.
    * This will perform transforms on the App.Config files for all the Ops projects to use a different Queue name in Azure.

The API project is created as a Owin Self Hosted API, and uses TopShelf to allow the project to run as a Console App or be installed as a Windows Service.
Because this is a Self Hosted Owin API you will need to add a http urlacl reservation on your machine to bind to the API Port.

Open a command prompt as an administrator and run the following command.

```
#!cmd

netsh http add urlacl url=http://+:50000 user=EVERYONE delegate=no
```

### Testing ###
When you launch the Integration Test, the Test Runner will execute Framework/TestAgent.cs to initialize the Tests. This will:

1. Configure the logging for the Test Runner
2. Load the Owin Startup class in the Endpoint.Api project in an Owin TestServer.
3. Use the Endpoint.Api.DependencyConfig.Container as the Dependency Container for the purposes of the test.
4. Try to drop any existing Test Databases using sqlcmd.exe.
5. Start an instance of the User Service's NServiceBus.Host 
6. Start an instance of the Animal Service's NServiceBus.Host
7. Wait for a Mutex to be created by the User Service's NServiceBus.Host to indicate that the Host has finished loading.
8. Wait for a Mutex to be created by the Animal Service's NServiceBus.Host to indicate that the Host has finished loading.
9. Register the handlers in the Test Project which subscribe to expected events published by both User and Animal Services.
10. A test from a Test class will run.
11. A test class inherits from **EventTesting** and calls "SetEventExpectation<T>(lambda)" where T is the Event we are expecting to receive and the lambda are the properties we are expecting to see on that message. This uses Reactive Extensions to watch a list of Events for a given Timeout.
12. When we call **VerifyEventExpectations** the EventTesting class is going to wait for the Event we expected or until the Timeout is raised.

This allows our Integration test to act as the API Client making the API calls and waiting for the system to process all the requests.

### Usage ###
Start up the endpoints.

* DemoGame.Ops.Api/bin/Release/DemoGame.Ops.Api.exe (The API Endpoint)
* DemoGame.Ops.User.Host/bin/Release/NServiceBus.Host.exe (The User Service Host)
* DemoGame.Ops.Animal.Host/bin/Release/NServiceBus.Host.exe (The Animal Service Host)

1. Check the API is running and contactable **GET http://localhost:50000/status**
2. Create a User by putting Firstname and Lastname as json to **PUT http://localhost:50000/v1/user/{userid:guid}**
3. A user will be returned immediately, but is put on a bus to be persisted behind the scenes.
4. Check the User actually persisted to the Database **GET http://localhost:50000/v1/user/{userid:guid}**
5. Create an animal (Dog/Cat/Rabbit) by putting UserId and Name to **PUT http://localhost:50000/v1/animal/cat/{animalid:guid}**
6. Get the animal details **GET http://localhost:50000/v1/animal/{animalid:guid}**
7. Feed the animal (No body required) **PUT http://localhost:50000/v1/animal/feed/{animalid:guid}**
8. Get the animal details and ensure the animal NourishmentLevel has increased **GET http://localhost:50000/v1/animal/{animalid:guid}**
9. Pet the animal (No body required) **PUT http://localhost:50000/v1/animal/pet/{animalid:guid}**
10. Get the animal details and ensure the animal MoodLevel has increased **GET http://localhost:50000/v1/animal/{animalid:guid}**
11. Leave the animal alone for a few seconds and then get the animal details again and the Mood and Nourishment levels should have decreased **GET http://localhost:50000/v1/animal/{animalid:guid}**
12. If you leave the animal alone for long enough and get the details again, it should come back with Status: 2 (Dead)

### What is happening ###

#### Creating a user ####
When you put a User to /v1/user/{userid:guid}, a message is sent on the bus, using NServiceBus, to the "User Service" and an **Accepted** HttpStatus is returned with a UserModel simulating that the User has been created.

The User Service's Host receives the CreateUser message and will persist this in its database. This is stored in this service as information such as the Users name is not required anywhere else in the system. This also means that the User Service could handle authentication and other User related activities, which are not required by any other services.

Once the User Service has Created the user successfully in its database, it publishes a UserCreated message.

The "Animal Service" is interested in this event and subscribes to this event. When the Animal Service receives this message, it will persist the UserId in its own User table in its own database, to allow animals to be created for this user.

#### Create an animal ####
When you put an animal (Cat/Dog/Rabbit) to /v1/animal/{animalid:guid}, a message is sent on the bus and an **Accepted** HttpStatus is returned with an AnimalModel simulating that the Animal has been created.

The Animal Service's Host receives the CreateAnimal message and will persist this in its database in the Animal Table. Once this data has been persisted in the database, a BringAnimalToLife message is sent to itself, to start a Saga to manage the Animals Life Workflow, passing to the saga, the AnimalId, the Animals BurnRates and MaxThresholds.

When an Animal Saga is started by the BringAnimalToLife message:
1. The details from the AnimalId, BurnRates and MaxThresholds are stored as SagaData.
2. The Levels are stored on the SagaData and are set at a neutral level. 
3. A Timeout is initiated to IncreaseHunger after the animals NourishmentBurnRate.
4. A Timeout is initiated to LowerMood after the animals MoodBurnRate.

#### Feed an Animal ####

When A FeedAnimal message is received, (Put to the API /v1/animal/feed/{animalid:guid}):
1. The current UTC DateTime is stored on the SagaData as the LastFed DateTime.
2. If the NourishmentLevel can be increased, then the SagaData's NourishmentLevel is increased by one and a message is sent to PersistNourishmentLevel so that the host can save this new level to the Animal ReadModel in the Animal Table. This allows a user to get the Animal Details by looking up the Animal in the database, rather than trying to query the Saga as this is not recommended.
3. A new Timeout is initiated for the NourishmentBurnRate to Increase the hunger again after this BurnRate.

#### Pet an Animal ####

When A Pet Animal message is received, (Put to the API /v1/animal/pet/{animalid:guid}):
1. The current UTC DateTime is stored on the SagaData as the LastPet DateTime.
2. If the MoodLevel can be increased, then the SagaData's MoodLevel is increased by one and a message is sent to PersistMoodLevel so that the host can save this new level to the Animal ReadModel in the Animal Table. This allows a user to get the Animal Details by looking up the Animal in the database, rather than trying to query the Saga as this is not recommended.
3. A new Timeout is initiated for the MoodBurnRate to lower the mood again after this BurnRate.

#### Animal gets hungry ####

When A IncreaseHunger Timeout is receieved
1. The SagaData.LastFed DateTime is subtracted from UTC now to decide whether this timeout is still required to be executed. This is because a Timeout could have been sent a while ago, but a feed command had been received since.
2. If the level does indeed need to be reduced, then the SagaData's NourishmentLevel is decreased by one. The timeout will also check both the MoodLevel and the NourishmentLevel to see if either are 0, and if so this will send a KillAnimal command and mark the Saga as complete. If the animal is not to be killed, then a message is sent to PersistNourishmentLevel so that the host can save this new level to the Animal ReadModel in the Animal Table. This allows a user to get the Animal Details by looking up the Animal in the database, rather than trying to query the Saga as this is not recommended.
3. A new Timeout is initiated for the NourishmentBurnRate to increase the hunger again after this BurnRate.

#### Animal gets bored ####

When A LowerMood Timeout is receieved
1. The SagaData.LastPet DateTime is subtracted from UTC now to decide whether this timeout is still required to be executed. This is because a Timeout could have been sent a while ago, but a pet command had been received since.
2. If the level does indeed need to be reduced, then the SagaData's MoodLevel is decreased by one. The timeout will also check both the MoodLevel and the NourishmentLevel to see if either are 0, and if so this will send a KillAnimal command and mark the Saga as complete. If the animal is not to be killed, then a message is sent to PersistMoodLevel so that the host can save this new level to the Animal ReadModel in the Animal Table. This allows a user to get the Animal Details by looking up the Animal in the database, rather than trying to query the Saga as this is not recommended.
3. A new Timeout is initiated for the MoodBurnRate to increase the mood again after this BurnRate.