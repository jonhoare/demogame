﻿using System;

namespace DemoGame.User.Contract.V1.Commands
{
    public class CreateUserV1
    {
        private CreateUserV1()
        {
        }

        public CreateUserV1(Guid userId, string firstname, string lastname)
        {
            UserId = userId;
            Firstname = firstname;
            Lastname = lastname;
        }

        public Guid UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}