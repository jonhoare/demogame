﻿using System;

namespace DemoGame.User.Contract.V1.Events
{
    public class UserCreatedV1
    {
        private UserCreatedV1()
        {
        }

        public UserCreatedV1(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId { get; set; }
    }
}