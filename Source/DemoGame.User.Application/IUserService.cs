﻿using System;

namespace DemoGame.User.Application
{
    public interface IUserService
    {
        void CreateUser(Guid userId, string firstname, string lastname);
        Domain.Models.User GetUser(Guid userId);
    }
}