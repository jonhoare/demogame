﻿using System;
using DemoGame.User.Domain.Repositories;

namespace DemoGame.User.Application
{
    public sealed class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        void IUserService.CreateUser(Guid userId, string firstname, string lastname)
        {
            var user = _userRepository.GetUser(userId);
            if (user != null)
                return;

            _userRepository.CreateUser(userId, firstname, lastname);
            _userRepository.SaveChanges();
        }
        
        Domain.Models.User IUserService.GetUser(Guid userId)
        {
            return _userRepository.GetUser(userId);
        }
    }
}
