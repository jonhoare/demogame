﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoGame.Animal.Domain.Repositories;

namespace DemoGame.Animal.Application
{
    public sealed class AnimalService : IAnimalService
    {
        private readonly IAnimalRepository _animalRepository;

        public AnimalService(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }

        Domain.Models.Animal IAnimalService.CreateDog(Guid animalId, Guid userId, string name)
        {
            var animal = _animalRepository.GetAnimal(animalId);
            if (animal != null)
                return animal;

            animal = _animalRepository.CreateDog(animalId, userId, name);
            _animalRepository.SaveChanges();

            return animal;
        }

        Domain.Models.Animal IAnimalService.CreateCat(Guid animalId, Guid userId, string name)
        {
            var animal = _animalRepository.GetAnimal(animalId);
            if (animal != null)
                return animal;

            animal = _animalRepository.CreateCat(animalId, userId, name);
            _animalRepository.SaveChanges();

            return animal;
        }

        Domain.Models.Animal IAnimalService.CreateRabbit(Guid animalId, Guid userId, string name)
        {
            var animal = _animalRepository.GetAnimal(animalId);
            if (animal != null)
                return animal;

            animal = _animalRepository.CreateRabbit(animalId, userId, name);
            _animalRepository.SaveChanges();

            return animal;
        }
        
        Domain.Models.Animal IAnimalService.GetAnimal(Guid animalId)
        {
            return _animalRepository.GetAnimal(animalId);
        }


        IList<Domain.Models.Animal> IAnimalService.GetAnimals(Guid userId)
        {
            return _animalRepository.GetAnimalsByUserId(userId).ToList();
        }
        
        void IAnimalService.SetMoodLevel(Guid animalId, int moodLevel)
        {
            var animal = _animalRepository.GetAnimal(animalId);
            animal.SetMoodLevel(moodLevel);

            _animalRepository.SaveChanges();
        }

        void IAnimalService.SetNourishmentLevel(Guid animalId, int nourishmentLevel)
        {
            var animal = _animalRepository.GetAnimal(animalId);
            animal.SetNourishmentLevel(nourishmentLevel);

            _animalRepository.SaveChanges();
        }

        void IAnimalService.KillAnimal(Guid animalId)
        {
            var animal = _animalRepository.GetAnimal(animalId);
            animal.Kill();

            _animalRepository.SaveChanges();
        }
    }
}