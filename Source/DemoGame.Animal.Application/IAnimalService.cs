﻿using System;
using System.Collections.Generic;

namespace DemoGame.Animal.Application
{
    public interface IAnimalService
    {
        Domain.Models.Animal CreateDog(Guid animalId, Guid userId, string name);
        Domain.Models.Animal CreateCat(Guid animalId, Guid userId, string name);
        Domain.Models.Animal CreateRabbit(Guid animalId, Guid userId, string name);

        void SetMoodLevel(Guid animalId, int moodLevel);
        void SetNourishmentLevel(Guid animalId, int nourishmentLevel);
        void KillAnimal(Guid animalId);

        Domain.Models.Animal GetAnimal(Guid animalId);
        IList<Domain.Models.Animal> GetAnimals(Guid userId);
    }
}