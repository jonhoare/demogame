﻿using Autofac;

namespace DemoGame.Animal.Application
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<AnimalService>().As<IAnimalService>();
        }
    }
}