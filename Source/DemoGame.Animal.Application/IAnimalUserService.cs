using System;

namespace DemoGame.Animal.Application
{
    public interface IUserService
    {
        void CreateUser(Guid userId);
        Domain.Models.User GetUser(Guid userId);
    }
}