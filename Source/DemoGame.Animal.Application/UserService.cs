﻿using System;
using DemoGame.Animal.Domain.Repositories;

namespace DemoGame.Animal.Application
{
    public sealed class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        void IUserService.CreateUser(Guid userId)
        {
            var user = _userRepository.GetUser(userId);
            if (user != null)
                return;

            _userRepository.CreateUser(userId);
            _userRepository.SaveChanges();
        }

        Domain.Models.User IUserService.GetUser(Guid userId)
        {
            return _userRepository.GetUser(userId);
        }
    }
}