﻿using System;
using Humanizer;

namespace DemoGame.Animal.Domain.Models
{
    public sealed class Dog : Animal
    {
        private Dog()
            : base()
        {
        }

        public Dog(Guid animalId, Guid userId, string name)
            : base(animalId, userId, name)
        {
        }

        public override TimeSpan NourishmentBurnRate { get { return 5.Seconds(); } }
        public override TimeSpan MoodBurnRate { get { return 5.Seconds(); } }
    }
}