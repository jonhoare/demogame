﻿using System;
using System.Collections.Generic;

namespace DemoGame.Animal.Domain.Models
{
    public class User
    {
        private User()
        {
        }

        public User(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId { get; private set; }

        public ICollection<Animal> Animals { get; set; }
    }
}