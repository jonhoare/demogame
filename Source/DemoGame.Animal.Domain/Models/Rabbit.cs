﻿using System;
using Humanizer;

namespace DemoGame.Animal.Domain.Models
{
    public sealed class Rabbit : Animal
    {
        private Rabbit()
            : base()
        {
        }

        public Rabbit(Guid animalId, Guid userId, string name)
            : base(animalId, userId, name)
        {
        }

        public override TimeSpan NourishmentBurnRate { get { return 10.Minutes(); } }
        public override TimeSpan MoodBurnRate { get { return 10.Minutes(); } }
    }
}