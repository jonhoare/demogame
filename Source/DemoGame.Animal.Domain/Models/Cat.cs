﻿using System;
using Humanizer;

namespace DemoGame.Animal.Domain.Models
{
    public sealed class Cat : Animal
    {
        private Cat()
            : base()
        {
        }

        public Cat(Guid animalId, Guid userId, string name)
            : base(animalId, userId, name)
        {
        }

        public override TimeSpan NourishmentBurnRate { get { return 10.Seconds(); } }
        public override TimeSpan MoodBurnRate { get { return 10.Seconds(); } }
    }
}