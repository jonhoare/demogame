﻿using System;

namespace DemoGame.Animal.Domain.Models
{
    public abstract class Animal
    {
        public const int MaxNourishment = 10;
        public const int MaxHappiness = 10;

        public static int StartingNourishmentLevel { get { return MaxNourishment / 2; } }
        public static int StartingMoodLevel { get { return MaxHappiness / 2; } }

        public enum LifeStatus
        {
            Alive = 1,
            Dead = 2
        }

        internal Animal()
        {
        }

        protected Animal(Guid animalId, Guid userId, string name)
        {
            AnimalId = animalId;
            UserId = userId;
            Name = name;
            NourishmentLevel = StartingNourishmentLevel;
            MoodLevel = StartingMoodLevel;
            Status = LifeStatus.Alive;
        }

        public Guid AnimalId { get; private set; }
        public Guid UserId { get; private set; }
        public string Name { get; private set; }
        public int NourishmentLevel { get; private set; }
        public int MoodLevel { get; private set; }
        public LifeStatus Status { get; private set; }

        public abstract TimeSpan NourishmentBurnRate { get; }
        public abstract TimeSpan MoodBurnRate { get; }

        public virtual User User { get; set; }
        
        public void Kill()
        {
            MoodLevel = 0;
            NourishmentLevel = 0;
            Status = LifeStatus.Dead;
        }

        public void SetMoodLevel(int moodLevel)
        {
            MoodLevel = moodLevel;
        }

        public void SetNourishmentLevel(int nourishmentLevel)
        {
            NourishmentLevel = nourishmentLevel;
        }
    }
}