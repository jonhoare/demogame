﻿using System;
using System.Collections.Generic;

namespace DemoGame.Animal.Domain.Repositories
{
    public interface IAnimalRepository
    {
        Models.Animal CreateDog(Guid animalId, Guid userId, string name);
        Models.Animal CreateCat(Guid animalId, Guid userId, string name);
        Models.Animal CreateRabbit(Guid animalId, Guid userId, string name);

        Models.Animal GetAnimal(Guid animalId);
        IEnumerable<Models.Animal> GetAnimalsByUserId(Guid userId);

        void SaveChanges();
    }
}