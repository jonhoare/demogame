﻿using System;

namespace DemoGame.Animal.Domain.Repositories
{
    public interface IUserRepository
    {
        Models.User CreateUser(Guid userId);
        Models.User GetUser(Guid userId);

        void SaveChanges();
    }
}