﻿using System;
using DemoGame.User.Domain.Repositories;

namespace DemoGame.User.Persistence
{
    public sealed class UserRepository : IUserRepository
    {
        private readonly UserDbContext _dbContext;

        public UserRepository(UserDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        User.Domain.Models.User IUserRepository.CreateUser(Guid userId, string firstname, string lastname)
        {
            return _dbContext.Users.Add(new User.Domain.Models.User(userId, firstname, lastname));
        }

        User.Domain.Models.User IUserRepository.GetUser(Guid userId)
        {
            return _dbContext.Users.Find(userId);
        }

        void IUserRepository.SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}