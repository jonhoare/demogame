﻿using Autofac;
using DemoGame.User.Domain.Repositories;

namespace DemoGame.User.Persistence
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserDbContext>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
        }
    }
}