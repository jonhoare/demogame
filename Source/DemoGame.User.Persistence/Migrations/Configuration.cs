using System.Data.Entity.Migrations;

namespace DemoGame.User.Persistence.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<UserDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(UserDbContext context)
        {
        }
    }
}
