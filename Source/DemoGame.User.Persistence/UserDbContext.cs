﻿using System.Data.Entity;

namespace DemoGame.User.Persistence
{
    public class UserDbContext : DbContext
    {
        public UserDbContext()
            : base("UserDbContext")
        {
        }

        public DbSet<User.Domain.Models.User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}