﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using Serilog;
using Configuration = DemoGame.User.Persistence.Migrations.Configuration;

namespace DemoGame.User.Persistence
{
    public class DatabaseConfig
    {
        public static void SetMigrationInitializer()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<UserDbContext, Configuration>());
        }

        public static void UpgradeDatabase()
        {
            Log.Debug("Upgrading Database");

            var connectionString = ConfigurationManager.ConnectionStrings["UserDbContext"];
            var configuration = new Configuration
            {
                TargetDatabase = new DbConnectionInfo(connectionString.ConnectionString, connectionString.ProviderName)
            };

            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }
    }
}