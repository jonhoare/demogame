using NServiceBus;

namespace DemoGame.Ops.Animal.Host
{
    public class EndpointConfig : IConfigureThisEndpoint
    {
        public void Customize(BusConfiguration configuration)
        {
            DemoGame.Animal.Endpoint.Framework.EndPointConfig.Configure(configuration);
        }
    }
}
