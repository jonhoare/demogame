﻿using System;
using System.Configuration;
using DemoGame.Animal.Persistence;
using NServiceBus;
using NServiceBus.Logging;
using NServiceBus.Serilog;
using Serilog;

namespace DemoGame.Animal.Endpoint.Framework
{
    public class EndPointConfig
    {
        public static string EndpointName = ConfigurationManager.AppSettings["EndpointName"];

        public static void Configure(BusConfiguration configuration)
        {
            LoggingConfig.Configure();
            DependencyConfig.Configure();

            DatabaseConfig.SetMigrationInitializer();
            DatabaseConfig.UpgradeDatabase();

            ConfigureNServiceBus(configuration);

            Log.Debug("Host is running");
        }

        private static void ConfigureNServiceBus(BusConfiguration configuration)
        {
            LogManager.Use<SerilogFactory>();

            configuration.LicensePath(string.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, "License.xml"));
            configuration.EndpointName(EndpointName);
            configuration.ScaleOut().UseSingleBrokerQueue();
            configuration.UseContainer<AutofacBuilder>(c => c.ExistingLifetimeScope(DependencyConfig.Container));
            configuration.EnableInstallers();
            configuration.UseTransport<AzureServiceBusTransport>().ConnectionStringName("AzureServiceBus");
            configuration.UseSerialization<JsonSerializer>();
            configuration.UsePersistence<InMemoryPersistence>();
            configuration.Conventions()
                .DefiningCommandsAs(
                    c => c.Namespace == "DemoGame.Animal.Contract.V1.Commands"
                )
                .DefiningEventsAs(
                    c => c.Namespace == "DemoGame.Animal.Contract.V1.Events"
                      || c.Namespace == "DemoGame.User.Contract.V1.Events"
                );
        }
    }
}