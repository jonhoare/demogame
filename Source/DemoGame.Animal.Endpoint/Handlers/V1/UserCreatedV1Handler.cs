﻿using DemoGame.Animal.Application;
using DemoGame.User.Contract.V1.Events;
using NServiceBus;
using Serilog;

namespace DemoGame.Animal.Endpoint.Handlers.V1
{
    public class UserCreatedV1Handler : IHandleMessages<UserCreatedV1>
    {
        private readonly IUserService _userService;
        private readonly IBus _bus;

        public UserCreatedV1Handler(IBus bus, IUserService userService)
        {
            _bus = bus;
            _userService = userService;
        }

        public void Handle(UserCreatedV1 message)
        {
            Log.Debug("Creating User: {0},", message.UserId);

            _userService.CreateUser(message.UserId);
        }
    }
}