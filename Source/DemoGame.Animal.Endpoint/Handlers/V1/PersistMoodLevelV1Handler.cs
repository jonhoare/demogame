﻿using DemoGame.Animal.Application;
using DemoGame.Animal.Contract.V1.Commands;
using DemoGame.Animal.Contract.V1.Events;
using NServiceBus;
using Serilog;

namespace DemoGame.Animal.Endpoint.Handlers.V1
{
    public class PersistMoodLevelV1Handler : IHandleMessages<PersistMoodLevelV1>
    {
        private readonly IAnimalService _animalService;
        private readonly IBus _bus;

        public PersistMoodLevelV1Handler(IBus bus, IAnimalService animalService)
        {
            _bus = bus;
            _animalService = animalService;
        }

        public void Handle(PersistMoodLevelV1 message)
        {
            Log.Debug("Persisting mood level for Animal: {0}", message.AnimalId);

            _animalService.SetMoodLevel(message.AnimalId, message.MoodLevel);

            _bus.Publish(new MoodLevelPersistedV1(message.AnimalId));
        }
    }
}