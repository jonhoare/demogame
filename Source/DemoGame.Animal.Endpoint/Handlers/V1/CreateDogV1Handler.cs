﻿using DemoGame.Animal.Application;
using DemoGame.Animal.Contract.V1.Commands;
using DemoGame.Animal.Contract.V1.Events;
using NServiceBus;
using Serilog;

namespace DemoGame.Animal.Endpoint.Handlers.V1
{
    public class CreateDogV1Handler : IHandleMessages<CreateDogV1>
    {
        private readonly IAnimalService _animalService;
        private readonly IBus _bus;

        public CreateDogV1Handler(IBus bus, IAnimalService animalService)
        {
            _bus = bus;
            _animalService = animalService;
        }

        public void Handle(CreateDogV1 message)
        {
            Log.Debug("Creating Dog: {0}, UserId: {1}", message.AnimalId, message.UserId);

            var animal = _animalService.CreateDog(message.AnimalId, message.UserId, message.Name);

            _bus.Send(new BringAnimalToLifeV1(
                animal.AnimalId,
                animal.NourishmentBurnRate,
                animal.MoodBurnRate,
                Domain.Models.Animal.MaxHappiness,
                Domain.Models.Animal.MaxNourishment));

            _bus.Publish(new DogCreatedV1(message.UserId, message.AnimalId));
        }
    }
}