﻿using DemoGame.Animal.Application;
using DemoGame.Animal.Contract.V1.Commands;
using DemoGame.Animal.Contract.V1.Events;
using NServiceBus;
using Serilog;

namespace DemoGame.Animal.Endpoint.Handlers.V1
{
    public class PersistNourishmentLevelV1Handler : IHandleMessages<PersistNourishmentLevelV1>
    {
        private readonly IAnimalService _animalService;
        private readonly IBus _bus;

        public PersistNourishmentLevelV1Handler(IBus bus, IAnimalService animalService)
        {
            _bus = bus;
            _animalService = animalService;
        }

        public void Handle(PersistNourishmentLevelV1 message)
        {
            Log.Debug("Persisting nourishment level for Animal: {0}", message.AnimalId);

            _animalService.SetNourishmentLevel(message.AnimalId, message.NourishmentLevel);

            _bus.Publish(new NourishmentLevelPersistedV1(message.AnimalId));
        }
    }
}