﻿using DemoGame.Animal.Application;
using DemoGame.Animal.Contract.V1.Commands;
using DemoGame.Animal.Contract.V1.Events;
using NServiceBus;
using Serilog;

namespace DemoGame.Animal.Endpoint.Handlers.V1
{
    public class KillAnimalV1Handler : IHandleMessages<KillAnimalV1>
    {
        private readonly IAnimalService _animalService;
        private IBus _bus;

        public KillAnimalV1Handler(IBus bus, IAnimalService animalService)
        {
            _bus = bus;
            _animalService = animalService;
        }

        public void Handle(KillAnimalV1 message)
        {
            Log.Debug("Killing Animal: {0}", message.AnimalId);

            _animalService.KillAnimal(message.AnimalId);
            _bus.Publish(new AnimalKilledV1(message.AnimalId));
        }
    }
}