﻿using System;
using DemoGame.Animal.Contract.V1.Commands;
using NServiceBus;
using NServiceBus.Saga;

namespace DemoGame.Animal.Endpoint.Sagas
{
    public class AnimalSaga : Saga<AnimalSagaData>,
        IAmStartedByMessages<BringAnimalToLifeV1>,
        IHandleMessages<FeedAnimalV1>,
        IHandleMessages<PetAnimalV1>,
        IHandleTimeouts<IncreaseHungerV1>,
        IHandleTimeouts<LowerMoodV1>
    {
        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<AnimalSagaData> mapper)
        {
            mapper.ConfigureMapping<BringAnimalToLifeV1>(message => message.AnimalId)
                .ToSaga(sagaData => sagaData.AnimalId);

            mapper.ConfigureMapping<FeedAnimalV1>(message => message.AnimalId)
                .ToSaga(sagaData => sagaData.AnimalId);

            mapper.ConfigureMapping<PetAnimalV1>(message => message.AnimalId)
                .ToSaga(sagaData => sagaData.AnimalId);
        }

        public void Handle(BringAnimalToLifeV1 message)
        {
            Data.AnimalId = message.AnimalId;
            Data.NourishmentBurnRate = message.NourishmentBurnRate;
            Data.MoodBurnRate = message.MoodBurnRate;
            Data.MaxMoodLevel = message.MaxMoodLevel;
            Data.MaxNourishmentLevel = message.MaxNourishmentLevel;
            Data.MoodLevel = message.MaxMoodLevel / 2;
            Data.NourishmentLevel = message.MaxNourishmentLevel / 2;

            RequestTimeout(Data.NourishmentBurnRate, new IncreaseHungerV1(message.AnimalId));
            RequestTimeout(Data.MoodBurnRate, new LowerMoodV1(message.AnimalId));
        }

        public void Handle(FeedAnimalV1 message)
        {
            Data.LastFed = DateTime.UtcNow;

            if (Data.NourishmentLevel < Data.MaxNourishmentLevel)
            {
                Data.NourishmentLevel += 1;
                Bus.Send(new PersistNourishmentLevelV1(message.AnimalId, Data.NourishmentLevel));
            }

            RequestTimeout(Data.NourishmentBurnRate, new IncreaseHungerV1(message.AnimalId));
        }

        public void Handle(PetAnimalV1 message)
        {
            Data.LastPet = DateTime.UtcNow;

            if (Data.MoodLevel < Data.MaxMoodLevel)
            {
                Data.MoodLevel += 1;
                Bus.Send(new PersistMoodLevelV1(message.AnimalId, Data.MoodLevel));
            }

            RequestTimeout(Data.MoodBurnRate, new LowerMoodV1(message.AnimalId));
        }

        public void Timeout(IncreaseHungerV1 message)
        {
            var hasBeenFedRecently = DateTime.UtcNow.Subtract(Data.LastFed) < Data.NourishmentBurnRate;

            if (!hasBeenFedRecently)
            {
                Data.NourishmentLevel -= 1;

                if (CheckAndKillIfRequired())
                    return;

                Bus.Send(new PersistNourishmentLevelV1(Data.AnimalId, Data.NourishmentLevel));
                RequestTimeout(Data.NourishmentBurnRate, new IncreaseHungerV1(message.AnimalId));   
            }
        }

        public void Timeout(LowerMoodV1 message)
        {
            var hasBeenPetRecently = DateTime.UtcNow.Subtract(Data.LastPet) < Data.MoodBurnRate;

            if (!hasBeenPetRecently)
            {
                Data.MoodLevel -= 1;

                if (CheckAndKillIfRequired())
                    return;

                Bus.Send(new PersistMoodLevelV1(Data.AnimalId, Data.MoodLevel));
                RequestTimeout(Data.MoodBurnRate, new LowerMoodV1(message.AnimalId));
            }
        }

        private bool CheckAndKillIfRequired()
        {
            var animalHasStarved = Data.NourishmentLevel <= 0 ;
            var animalHasDiedOfBoredom = Data.MoodLevel <= 0;
            var animalShouldDie = animalHasStarved || animalHasDiedOfBoredom;

            if (animalShouldDie)
            {
                Data.NourishmentLevel = 0;
                Data.MoodLevel = 0;

                Bus.Send(new KillAnimalV1(Data.AnimalId));

                MarkAsComplete();
            }

            return animalShouldDie;
        }
    }
}