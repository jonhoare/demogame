﻿using System;
using NServiceBus.Saga;

namespace DemoGame.Animal.Endpoint.Sagas
{
    public class AnimalSagaData : IContainSagaData
    {
        public Guid Id { get; set; }
        public string OriginalMessageId { get; set; }
        public string Originator { get; set; }

        [Unique]
        public Guid AnimalId { get; set; }
        public TimeSpan NourishmentBurnRate { get; set; }
        public TimeSpan MoodBurnRate { get; set; }
        public int MaxNourishmentLevel { get; set; }
        public int MaxMoodLevel { get; set; }
        public int MoodLevel { get; set; }
        public int NourishmentLevel { get; set; }
        public DateTime LastFed { get; set; }
        public DateTime LastPet { get; set; }
    }
}