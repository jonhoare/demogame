namespace DemoGame.Animal.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animals",
                c => new
                    {
                        AnimalId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Name = c.String(),
                        NourishmentLevel = c.Int(nullable: false),
                        MoodLevel = c.Int(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.AnimalId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Animals", "UserId", "dbo.Users");
            DropIndex("dbo.Animals", new[] { "UserId" });
            DropTable("dbo.Users");
            DropTable("dbo.Animals");
        }
    }
}
