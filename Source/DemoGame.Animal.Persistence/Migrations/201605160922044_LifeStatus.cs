namespace DemoGame.Animal.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LifeStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Animals", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Animals", "Status");
        }
    }
}
