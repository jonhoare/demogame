using System.Data.Entity.Migrations;

namespace DemoGame.Animal.Persistence.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AnimalDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AnimalDbContext context)
        {
        }
    }
}
