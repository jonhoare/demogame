﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoGame.Animal.Domain.Models;
using DemoGame.Animal.Domain.Repositories;

namespace DemoGame.Animal.Persistence
{
    public sealed class AnimalRepository : IAnimalRepository
    {
        private readonly AnimalDbContext _dbContext;

        public AnimalRepository(AnimalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        Animal.Domain.Models.Animal IAnimalRepository.CreateDog(Guid animalId, Guid userId, string name)
        {
            return _dbContext.Animals.Add(new Dog(animalId, userId, name));
        }

        Animal.Domain.Models.Animal IAnimalRepository.CreateCat(Guid animalId, Guid userId, string name)
        {
            return _dbContext.Animals.Add(new Cat(animalId, userId, name));
        }

        Animal.Domain.Models.Animal IAnimalRepository.CreateRabbit(Guid animalId, Guid userId, string name)
        {
            return _dbContext.Animals.Add(new Rabbit(animalId, userId, name));
        }

        Animal.Domain.Models.Animal IAnimalRepository.GetAnimal(Guid animalId)
        {
            return _dbContext.Animals.Find(animalId);
        }

        void IAnimalRepository.SaveChanges()
        {
            _dbContext.SaveChanges();
        }
        
        IEnumerable<Animal.Domain.Models.Animal> IAnimalRepository.GetAnimalsByUserId(Guid userId)
        {
            return _dbContext.Animals.Where(a => a.UserId == userId);
        }
    }
}