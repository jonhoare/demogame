﻿using System;
using DemoGame.Animal.Domain.Models;
using DemoGame.Animal.Domain.Repositories;

namespace DemoGame.Animal.Persistence
{
    public sealed class UserRepository : IUserRepository
    {
        private readonly AnimalDbContext _dbContext;

        public UserRepository(AnimalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        User IUserRepository.CreateUser(Guid userId)
        {
            return _dbContext.Users.Add(new User(userId));
        }

        User IUserRepository.GetUser(Guid userId)
        {
            return _dbContext.Users.Find(userId);
        }

        void IUserRepository.SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}