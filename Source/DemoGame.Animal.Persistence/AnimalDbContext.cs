﻿using System.Data.Entity;
using DemoGame.Animal.Domain.Models;

namespace DemoGame.Animal.Persistence
{
    public class AnimalDbContext : DbContext
    {
        public AnimalDbContext()
            : base("AnimalDbContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Domain.Models.Animal> Animals { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Domain.Models.Animal>()
                .HasRequired(a => a.User)
                .WithMany(a => a.Animals)
                .HasForeignKey(a => a.UserId);

            base.OnModelCreating(modelBuilder);
        }
    }
}