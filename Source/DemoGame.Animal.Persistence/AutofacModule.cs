﻿using Autofac;
using DemoGame.Animal.Domain.Repositories;
//using DemoGame.User.Domain.Repositories;

namespace DemoGame.Animal.Persistence
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AnimalDbContext>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<AnimalRepository>().As<IAnimalRepository>();
        }
    }
}