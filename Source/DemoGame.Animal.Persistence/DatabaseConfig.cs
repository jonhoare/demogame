﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using Serilog;
using Configuration = DemoGame.Animal.Persistence.Migrations.Configuration;

namespace DemoGame.Animal.Persistence
{
    public class DatabaseConfig
    {
        public static void SetMigrationInitializer()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AnimalDbContext, Configuration>());
        }

        public static void UpgradeDatabase()
        {
            Log.Debug("Upgrading Database");

            var connectionString = ConfigurationManager.ConnectionStrings["AnimalDbContext"];
            var configuration = new Configuration
            {
                TargetDatabase = new DbConnectionInfo(connectionString.ConnectionString, connectionString.ProviderName)
            };

            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }
    }
}