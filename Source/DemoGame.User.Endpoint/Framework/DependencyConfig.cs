﻿using Autofac;

namespace DemoGame.User.Endpoint.Framework
{
    public class DependencyConfig
    {
        public static IContainer Container { get; private set; }

        public static void Configure()
        {
            var builder = new ContainerBuilder();
            
            builder.RegisterModule<Application.AutofacModule>();
            builder.RegisterModule<Persistence.AutofacModule>();

            Container = builder.Build();
        }
    }
}