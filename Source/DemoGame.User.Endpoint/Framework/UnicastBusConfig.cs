﻿using NServiceBus.Config;
using NServiceBus.Config.ConfigurationSource;

namespace DemoGame.User.Endpoint.Framework
{
    public class UnicastBusConfig : IProvideConfiguration<NServiceBus.Config.UnicastBusConfig>
    {
        public NServiceBus.Config.UnicastBusConfig GetConfiguration()
        {
            return new NServiceBus.Config.UnicastBusConfig
            {
                MessageEndpointMappings = new MessageEndpointMappingCollection
                {
                    new MessageEndpointMapping
                    {
                        AssemblyName = "DemoGame.User.Contract",
                        Namespace = "DemoGame.User.Contract.V1.Commands",
                        Endpoint = EndPointConfig.EndpointName
                    },
                    new MessageEndpointMapping
                    {
                        AssemblyName = "DemoGame.User.Contract",
                        Namespace = "DemoGame.User.Contract.V1.Events",
                        Endpoint = EndPointConfig.EndpointName
                    }
                }
            };
        }
    }
}