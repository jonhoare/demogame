﻿using DemoGame.User.Application;
using DemoGame.User.Contract.V1.Commands;
using DemoGame.User.Contract.V1.Events;
using NServiceBus;
using Serilog;

namespace DemoGame.User.Endpoint.Handlers.V1
{
    public class CreateUserV1Handler : IHandleMessages<CreateUserV1>
    {
        private readonly IUserService _userService;
        private readonly IBus _bus;

        public CreateUserV1Handler(IBus bus, IUserService userService)
        {
            _bus = bus;
            _userService = userService;
        }

        public void Handle(CreateUserV1 message)
        {
            Log.Debug("Creating User: {0}", message.UserId);

            _userService.CreateUser(message.UserId, message.Firstname, message.Lastname);

            _bus.Publish(new UserCreatedV1(message.UserId));
        }
    }
}