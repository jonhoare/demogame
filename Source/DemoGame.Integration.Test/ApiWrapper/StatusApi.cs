﻿using System.Net;
using System.Net.Http;
using DemoGame.Integration.Test.Framework;

namespace DemoGame.Integration.Test.ApiWrapper
{
    public class StatusApi
    {
        private readonly string _baseUrl = "http://localhost";
        private readonly RestService _restService;

        public StatusApi()
            : this(new HttpClient())
        {
        }

        public StatusApi(HttpClient httpClient)
        {
            _restService = new RestService(httpClient);
        }

        public HttpStatusCode GetStatus()
        {
            var url = string.Format("{0}/status", _baseUrl);

            var response = _restService.Get(url);

            return response.StatusCode;
        }
    }
}