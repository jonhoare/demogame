﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using DemoGame.Integration.Test.Exceptions;
using DemoGame.Integration.Test.Framework;

namespace DemoGame.Integration.Test.ApiWrapper
{
    public class AnimalApi
    {
        private readonly string _baseUrl = "http://localhost";
        private readonly RestService _restService;

        public AnimalApi()
            : this(new HttpClient())
        {
        }

        public AnimalApi(HttpClient httpClient)
        {
            _restService = new RestService(httpClient);
        }

        public AnimalModel CreateDog(Guid animalId, Guid userId, string name)
        {
            return CreateAnimal("dog", animalId, userId, name);
        }

        public AnimalModel CreateCat(Guid animalId, Guid userId, string name)
        {
            return CreateAnimal("cat", animalId, userId, name);
        }

        public AnimalModel CreateRabbit(Guid animalId, Guid userId, string name)
        {
            return CreateAnimal("rabbit", animalId, userId, name);
        }

        private AnimalModel CreateAnimal(string animalType, Guid animalId, Guid userId, string name)
        {
            var url = string.Format("{0}/v1/animal/{1}/{2}", _baseUrl, animalType, animalId);

            var content = new
            {
                UserId = userId,
                Name = name
            };

            var response = _restService.Put(url, content);

            if (!response.IsSuccessStatusCode)
                throw new UnexpectedStatusCodeException(response);

            return response.Content.ReadAsAsync<AnimalModel>().Result;
        }

        public AnimalModel GetAnimal(Guid animalId)
        {
            var url = string.Format("{0}/v1/animal/{1}", _baseUrl, animalId);

            var response = _restService.Get(url);

            if (!response.IsSuccessStatusCode)
                throw new UnexpectedStatusCodeException(response);

            return response.Content.ReadAsAsync<AnimalModel>().Result;
        }

        public List<AnimalModel> GetAnimals(Guid userId)
        {
            var url = string.Format("{0}/v1/animal/user/{1}", _baseUrl, userId);

            var response = _restService.Get(url);

            if (!response.IsSuccessStatusCode)
                throw new UnexpectedStatusCodeException(response);

            return response.Content.ReadAsAsync<List<AnimalModel>>().Result;
        }

        public AnimalModel Feed(Guid animalId)
        {
            var url = string.Format("{0}/v1/animal/feed/{1}", _baseUrl, animalId);

            var content = new { };

            var response = _restService.Put(url, content);

            if (!response.IsSuccessStatusCode)
                throw new UnexpectedStatusCodeException(response);

            return response.Content.ReadAsAsync<AnimalModel>().Result;
        }

        public AnimalModel Pet(Guid animalId)
        {
            var url = string.Format("{0}/v1/animal/pet/{1}", _baseUrl, animalId);

            var content = new { };

            var response = _restService.Put(url, content);

            if (!response.IsSuccessStatusCode)
                throw new UnexpectedStatusCodeException(response);

            return response.Content.ReadAsAsync<AnimalModel>().Result;
        }
    }

    public class AnimalModel
    {
        public enum LifeStatus
        {
            Alive = 1,
            Dead = 2
        }

        public Guid AnimalId { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int NourishmentLevel { get; set; }
        public int MoodLevel { get; set; }
        public LifeStatus Status { get; set; }
    }
}