﻿using System;
using System.Net.Http;
using DemoGame.Integration.Test.Exceptions;
using DemoGame.Integration.Test.Framework;

namespace DemoGame.Integration.Test.ApiWrapper
{
    public class UserApi
    {
        private readonly string _baseUrl = "http://localhost";
        private readonly RestService _restService;

        public UserApi()
            : this(new HttpClient())
        {
        }

        public UserApi(HttpClient httpClient)
        {
            _restService = new RestService(httpClient);
        }

        public HttpResponseMessage CreateUser(Guid userId, string firstname, string lastname)
        {
            var url = string.Format("{0}/v1/user/{1}", _baseUrl, userId);

            var content = new
            {
                Firstname = firstname,
                Lastname = lastname
            };

            return _restService.Put(url, content);
        }

        public UserModel GetUser(Guid userId)
        {
            var url = string.Format("{0}/v1/user/{1}", _baseUrl, userId);

            var response = _restService.Get(url);

            if (!response.IsSuccessStatusCode)
                throw new UnexpectedStatusCodeException(response);

            return response.Content.ReadAsAsync<UserModel>().Result;
        }
    }

    public class UserModel
    {
        public Guid UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}