﻿using System;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace DemoGame.Integration.Test.Exceptions
{
    [Serializable]
    public class UnexpectedStatusCodeException : Exception
    {
        public string HttpMethod { get; private set; }
        public string RequestUri { get; private set; }
        public string StatusCode { get; private set; }

        public UnexpectedStatusCodeException(HttpResponseMessage response)
            : base(string.Format("HttpStatusCode {0} is an unexpected response.", response.StatusCode))
        {
            HttpMethod = response.RequestMessage.Method.Method;
            RequestUri = response.RequestMessage.RequestUri.AbsoluteUri;
            StatusCode = string.Format("{0} {1}", (int)response.StatusCode, response.StatusCode);
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected new virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}