﻿using System;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.Integration.Test.Framework;
using NServiceBus;

namespace DemoGame.Integration.Test.Handlers
{
    public class NourishmentLevelPersistedV1Handler : IHandleMessages<NourishmentLevelPersistedV1>
    {
        public void Handle(NourishmentLevelPersistedV1 message)
        {
            Console.WriteLine("NourishmentLevelPersistedV1 event received.");
            EventTesting.ReportEvent(message);
        }
    }
}