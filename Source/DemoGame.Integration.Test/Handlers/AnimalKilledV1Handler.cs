﻿using System;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.Integration.Test.Framework;
using NServiceBus;

namespace DemoGame.Integration.Test.Handlers
{
    public class AnimalKilledV1Handler : IHandleMessages<AnimalKilledV1>
    {
        public void Handle(AnimalKilledV1 message)
        {
            Console.WriteLine("AnimalKilledV1 event received.");
            EventTesting.ReportEvent(message);
        }
    }
}