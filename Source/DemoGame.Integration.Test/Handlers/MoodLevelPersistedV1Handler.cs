﻿using System;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.Integration.Test.Framework;
using NServiceBus;

namespace DemoGame.Integration.Test.Handlers
{
    public class MoodLevelPersistedV1Handler : IHandleMessages<MoodLevelPersistedV1>
    {
        public void Handle(MoodLevelPersistedV1 message)
        {
            Console.WriteLine("MoodLevelPersistedV1 event received.");
            EventTesting.ReportEvent(message);
        }
    }
}