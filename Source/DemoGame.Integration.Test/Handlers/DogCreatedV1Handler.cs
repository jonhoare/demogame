﻿using System;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.Integration.Test.Framework;
using NServiceBus;

namespace DemoGame.Integration.Test.Handlers
{
    public class DogCreatedV1Handler : IHandleMessages<DogCreatedV1>
    {
        public void Handle(DogCreatedV1 message)
        {
            Console.WriteLine("DogCreatedV1 event received.");
            EventTesting.ReportEvent(message);
        }
    }
}