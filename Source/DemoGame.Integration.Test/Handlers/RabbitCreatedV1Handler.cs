﻿using System;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.Integration.Test.Framework;
using NServiceBus;

namespace DemoGame.Integration.Test.Handlers
{
    public class RabbitCreatedV1Handler : IHandleMessages<RabbitCreatedV1>
    {
        public void Handle(RabbitCreatedV1 message)
        {
            Console.WriteLine("RabbitCreatedV1 event received.");
            EventTesting.ReportEvent(message);
        }
    }
}