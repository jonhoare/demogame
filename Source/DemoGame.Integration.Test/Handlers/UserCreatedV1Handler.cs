﻿using System;
using DemoGame.Integration.Test.Framework;
using DemoGame.User.Contract.V1.Events;
using NServiceBus;

namespace DemoGame.Integration.Test.Handlers
{
    public class UserCreatedV1Handler : IHandleMessages<UserCreatedV1>
    {
        public void Handle(UserCreatedV1 message)
        {
            Console.WriteLine("UserCreatedV1 event received.");
            EventTesting.ReportEvent(message);
        }
    }
}