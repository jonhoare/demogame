﻿using System;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.Integration.Test.Framework;
using NServiceBus;

namespace DemoGame.Integration.Test.Handlers
{
    public class CatCreatedV1Handler : IHandleMessages<CatCreatedV1>
    {
        public void Handle(CatCreatedV1 message)
        {
            Console.WriteLine("CatCreatedV1 event received.");
            EventTesting.ReportEvent(message);
        }
    }
}