﻿using System.Diagnostics;

namespace DemoGame.Integration.Test.Framework
{
    public class DatabaseMigrations
    {
        public static void PrepareTestDatabases()
        {
            DeleteDatabase("DemoGame.Test.User");
            DeleteDatabase("DemoGame.Test.Animal");
        }

        private static void DeleteDatabase(string databaseName)
        {
            var deleteDbProcess = new Process
            {
                StartInfo =
                {
                    FileName = "sqlcmd",
                    Arguments =
                        string.Format("-S (localdb)\\v11.0 -E -Q \"IF EXISTS (SELECT name FROM sys.databases WHERE name = N'{0}')"
                                      + " ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;"
                                      +
                                      " IF EXISTS (SELECT name FROM sys.databases WHERE name = N'{0}') DROP DATABASE [{0}]\"",
                            databaseName),
                    UseShellExecute = true
                }
            };

            deleteDbProcess.Start();
            deleteDbProcess.WaitForExit(30000);
        }
    }
}