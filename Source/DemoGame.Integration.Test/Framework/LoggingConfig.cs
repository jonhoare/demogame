﻿using Serilog;
using Serilog.Enrichers;

namespace DemoGame.Integration.Test.Framework
{
    public class LoggingConfig
    {
        public static void Configure()
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .Enrich.FromLogContext()
                .Enrich.With<MachineNameEnricher>()
                .Enrich.With<ThreadIdEnricher>()
                .CreateLogger();
        }
    }
}