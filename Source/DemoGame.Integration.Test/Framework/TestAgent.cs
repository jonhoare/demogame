﻿using System.Net.Http;
using Autofac;
using DemoGame.Endpoint.Api.Framework;
using DemoGame.Integration.Test.Framework;
using Microsoft.Owin.Testing;
using NUnit.Framework;

namespace DemoGame.Integration.Test
{
    [SetUpFixture]
    public class TestAgent
    {
        public static TestServer Server;
        public static HttpClient HttpClient;
        public static IContainer GlobalLifetimeScope;

        [OneTimeSetUp]
        public void FixtureInit()
        {
            Endpoint.Api.Framework.LoggingConfig.Configure();

            ConfigureApiTestServer();

            GlobalLifetimeScope = DependencyConfig.Container;
            
            DatabaseMigrations.PrepareTestDatabases();
            
            UserHostProcess.StartRunningHostProcess();
            AnimalHostProcess.StartRunningHostProcess();

            UserHostProcess.WaitUntilRunningHostIsInitialised();
            AnimalHostProcess.WaitUntilRunningHostIsInitialised();

            RegisterSubscribers.RegisterSubscribersWithBusInstance();
        }

        private static void ConfigureApiTestServer()
        {
            Server = TestServer.Create<Startup>();
            HttpClient = Server.HttpClient;
        }

        [OneTimeTearDown]
        public void FixtureDispose()
        {
            UserHostProcess.StopRunningHostProcess();
            AnimalHostProcess.StopRunningHostProcess();
        }
    }
}
