﻿using Autofac;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.User.Contract.V1.Events;
using NServiceBus;

namespace DemoGame.Integration.Test.Framework
{
    public class RegisterSubscribers
    {
        public static void RegisterSubscribersWithBusInstance()
        {
            var bus = TestAgent.GlobalLifetimeScope.Resolve<IBus>();

            bus.Subscribe<UserCreatedV1>();
            bus.Subscribe<DogCreatedV1>();
            bus.Subscribe<CatCreatedV1>();
            bus.Subscribe<RabbitCreatedV1>();
            bus.Subscribe<NourishmentLevelPersistedV1>();
            bus.Subscribe<MoodLevelPersistedV1>();
            bus.Subscribe<AnimalKilledV1>();
        }
    }
}