﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace DemoGame.Integration.Test.Framework
{
    public class RestService
    {
        private readonly HttpClient _httpClient;

        public RestService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public HttpResponseMessage Get(string uri)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(uri),
                Method = HttpMethod.Get,
                Content = null
            };

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Put(string uri, object content)
        {
            var json = JsonConvert.SerializeObject(content);
            var body = new StringContent(json, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(uri),
                Method = HttpMethod.Put,
                Content = body
            };
            request.Headers.Accept.Clear();
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return _httpClient.SendAsync(request).Result;
        }

        public HttpResponseMessage Delete(string uri)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(uri),
                Method = HttpMethod.Delete,
                Content = null
            };

            return _httpClient.SendAsync(request).Result;
        }
    }
}