﻿using System.Diagnostics;
using System.Threading;
using DemoGame.Integration.Test.Properties;
using NUnit.Framework;

namespace DemoGame.Integration.Test.Framework
{
    public class AnimalHostProcess
    {
        public static Process NsbHostProcess;

        public static void StartRunningHostProcess()
        {
            NsbHostProcess = new Process
            {
                StartInfo =
                {
                    FileName =
                        string.Format(
                            @"{0}\..\..\..\DemoGame.Ops.Animal.Host\bin\Debug\NServiceBus.Host.exe",
                            TestContext.CurrentContext.TestDirectory)
                }
            };
            NsbHostProcess.Start();
        }

        public static void StopRunningHostProcess()
        {
            if (NsbHostProcess != null)
                NsbHostProcess.Kill();
        }

        public static void WaitUntilRunningHostIsInitialised()
        {
            var hostInstanceIsInitialising = true;
            while (hostInstanceIsInitialising)
            {
                if (IsHostInstanceRunning())
                    hostInstanceIsInitialising = false;

                else
                    Thread.Sleep(100);
            }
        }

        private static bool IsHostInstanceRunning()
        {
            try
            {
                Mutex.OpenExisting(Settings.Default.OpsAnimalHostMutexName);
                return true;
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                return false;
            }
        }
    }
}