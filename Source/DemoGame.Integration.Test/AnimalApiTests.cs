﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DemoGame.Animal.Contract.V1.Events;
using DemoGame.Integration.Test.ApiWrapper;
using DemoGame.Integration.Test.Exceptions;
using DemoGame.Integration.Test.Framework;
using DemoGame.User.Contract.V1.Events;
using Humanizer;
using NUnit.Framework;

namespace DemoGame.Integration.Test
{
    [TestFixture]
    public class AnimalApiTests : EventTesting
    {
        private UserApi _userApi;
        private AnimalApi _animalApi;

        [SetUp]
        public void Init()
        {
            _userApi = new UserApi(TestAgent.HttpClient);
            _animalApi = new AnimalApi(TestAgent.HttpClient);
        }

        private AnimalModel CreateDog(Guid animalId, Guid userId, string animalName)
        {
            SetEventExpectation<DogCreatedV1>(x =>
                x.UserId == userId &&
                x.AnimalId == animalId);

            var response = _animalApi.CreateDog(animalId, userId, animalName);

            VerifyEventExpectations();

            return response;
        }

        private AnimalModel CreateCat(Guid animalId, Guid userId, string animalName)
        {
            SetEventExpectation<CatCreatedV1>(x =>
                x.UserId == userId &&
                x.AnimalId == animalId);

            var response = _animalApi.CreateCat(animalId, userId, animalName);

            VerifyEventExpectations();

            return response;
        }

        private AnimalModel CreateRabbit(Guid animalId, Guid userId, string animalName)
        {
            SetEventExpectation<RabbitCreatedV1>(x =>
                x.UserId == userId &&
                x.AnimalId == animalId);

            var response = _animalApi.CreateRabbit(animalId, userId, animalName);

            VerifyEventExpectations();

            return response;
        }

        private void CreateUser(Guid userId, string firstname, string lastname)
        {
            SetEventExpectation<UserCreatedV1>(x => x.UserId == userId);

            _userApi.CreateUser(userId, firstname, lastname);

            VerifyEventExpectations();
        }

        [Test]
        public void UserCreateADogReturnsADogModel()
        {
            var userId = new Guid("1b22186b-7433-4144-8c56-4598d74d2ab6");
            var firstname = "Gary";
            var lastname = "Garther";

            var animalId = new Guid("171dde3d-6e5c-4c23-ae96-f3bc189eacb1");
            var animalName = "Sammy";
            
            CreateUser(userId, firstname, lastname);
            var dog = CreateDog(animalId, userId, animalName);

            Assert.AreEqual("Dog", dog.Type);
            Assert.AreEqual(animalId, dog.AnimalId);
            Assert.AreEqual(userId, dog.UserId);
            Assert.AreEqual(animalName, dog.Name);
        }

        [Test]
        public void UserCreateACatReturnsACatModel()
        {
            var userId = new Guid("4657877b-2fdf-42b1-b0f8-8b7f4da2257d");
            var firstname = "Pam";
            var lastname = "Pritchet";

            var animalId = new Guid("a00b1c98-8fbb-47e7-9be1-d98dd9bc343b");
            var animalName = "Catrina";

            CreateUser(userId, firstname, lastname);
            var cat = CreateCat(animalId, userId, animalName);

            Assert.AreEqual("Cat", cat.Type);
            Assert.AreEqual(animalId, cat.AnimalId);
            Assert.AreEqual(userId, cat.UserId);
            Assert.AreEqual(animalName, cat.Name);
        }

        [Test]
        public void UserCreateARabbitReturnsARabbitModel()
        {
            var userId = new Guid("73f132ba-6767-42f4-b63d-becd50e1dfc1");
            var firstname = "Jeremy";
            var lastname = "Jenkins";

            var animalId = new Guid("e81774ed-9a96-4554-b90d-9c05bad591c6");
            var animalName = "Patch";

            CreateUser(userId, firstname, lastname);
            var rabbit = CreateRabbit(animalId, userId, animalName);

            Assert.AreEqual("Rabbit", rabbit.Type);
            Assert.AreEqual(animalId, rabbit.AnimalId);
            Assert.AreEqual(userId, rabbit.UserId);
            Assert.AreEqual(animalName, rabbit.Name);
        }

        [Test]
        public void GetAKnownAnimalIdReturnsAnAnimal()
        {
            var userId = new Guid("82a1b066-b03e-48ed-958f-1db27f61c74a");
            var firstname = "Sarah";
            var lastname = "Sears";

            var animalId = new Guid("09d067d6-aa93-4b58-8e1e-8535cde0f7bc");
            var animalName = "Barney";

            CreateUser(userId, firstname, lastname);
            CreateDog(animalId, userId, animalName);

            var animal = _animalApi.GetAnimal(animalId);

            Assert.AreEqual(animalId, animal.AnimalId);
            Assert.AreEqual(userId, animal.UserId);
            Assert.AreEqual(animalName, animal.Name);
        }

        [Test]
        public void GetAnAnimalByAnUnknownIdThrowsAnUnexpectedException()
        {
            var animalId = new Guid("980ab40a-1015-433c-b5be-584186541ddc");

            try
            {
                _animalApi.GetAnimal(animalId);

                Assert.Fail("The code should be throwing an exception right about now!");
            }
            catch (UnexpectedStatusCodeException)
            {
                Assert.Pass();
            }
        }

        [Test]
        public void UserWith2DogsReturns2Animals()
        {
            var userId = new Guid("8851bc2a-5e36-4adb-b2cb-cc5e1a322930");
            var firstname = "Dan";
            var lastname = "Diggins";

            CreateUser(userId, firstname, lastname);

            var animals = new List<AnimalModel>
            {
                new AnimalModel { AnimalId = new Guid("9061331a-9cdc-48f1-a6cf-0f4a9cae7c6c"), UserId = userId, Name = "Brucey" },
                new AnimalModel { AnimalId = new Guid("706af354-353b-4be4-a7bd-b63014861d75"), UserId = userId, Name = "Charlie" }
            };

            animals.ForEach(a => CreateDog(a.AnimalId, a.UserId, a.Name));

            var userAnimals = _animalApi.GetAnimals(userId);

            animals.ForEach(a =>
            {
                var userAnimal = userAnimals.Single(ua => ua.AnimalId == a.AnimalId);

                Assert.AreEqual(a.AnimalId, userAnimal.AnimalId);
                Assert.AreEqual(a.UserId, userAnimal.UserId);
                Assert.AreEqual(a.Name, userAnimal.Name);
            });
        }

        [Test]
        public void UserWith1Cat2DogsAnd1RabbitReturns4Animals()
        {
            var userId = new Guid("9b743739-d840-4c5a-9fc1-04e60b85b4ad");
            var firstname = "Frank";
            var lastname = "Flint";

            CreateUser(userId, firstname, lastname);

            var cat = new AnimalModel
            {
                AnimalId = new Guid("890175bc-f1b5-4234-a5b0-76566de4451f"),
                UserId = userId,
                Name = "Ernie"
            };

            var dog1 = new AnimalModel
            {
                AnimalId = new Guid("0888096d-775c-48eb-8b18-9a3bb9f3395b"),
                UserId = userId,
                Name = "Fern"
            };

            var dog2 = new AnimalModel
            {
                AnimalId = new Guid("d079cb86-d65b-4b37-acf1-b190638b35e5"),
                UserId = userId,
                Name = "Scooby Doo"
            };

            var rabbit = new AnimalModel
            {
                AnimalId = new Guid("beaede2f-951d-4e04-9d9c-614d51aa668f"),
                UserId = userId,
                Name = "Thumper"
            };

            CreateDog(dog1.AnimalId, dog1.UserId, dog1.Name);
            CreateDog(dog2.AnimalId, dog2.UserId, dog2.Name);
            CreateCat(cat.AnimalId, cat.UserId, cat.Name);
            CreateRabbit(rabbit.AnimalId, rabbit.UserId, rabbit.Name);

            var userAnimals = _animalApi.GetAnimals(userId);

            Assert.AreEqual(4, userAnimals.Count);

            var dog1Result = userAnimals.First(d => d.AnimalId == dog1.AnimalId);
            Assert.AreEqual("Dog", dog1Result.Type);
            Assert.AreEqual(dog1.UserId, dog1Result.UserId);
            Assert.AreEqual(dog1.Name, dog1Result.Name);

            var dog2Result = userAnimals.First(d => d.AnimalId == dog2.AnimalId);
            Assert.AreEqual("Dog", dog2Result.Type);
            Assert.AreEqual(dog2.UserId, dog2Result.UserId);
            Assert.AreEqual(dog2.Name, dog2Result.Name);

            var catResult = userAnimals.First(c => c.AnimalId == cat.AnimalId);
            Assert.AreEqual("Cat", catResult.Type);
            Assert.AreEqual(cat.UserId, catResult.UserId);
            Assert.AreEqual(cat.Name, catResult.Name);

            var rabbitResult = userAnimals.First(r => r.AnimalId == rabbit.AnimalId);
            Assert.AreEqual("Rabbit", rabbitResult.Type);
            Assert.AreEqual(rabbit.UserId, rabbitResult.UserId);
            Assert.AreEqual(rabbit.Name, rabbitResult.Name);
        }

        [Test]
        public void FeedingAnAnimalIncreasesNourishmentLevelBy1()
        {
            var userId = new Guid("1bb6ace4-db9e-4e96-9a08-e50a3e74e25b");
            var firstname = "James";
            var lastname = "Jollington";

            var animalId = new Guid("3d11a364-0960-4cf3-9db8-bba340d6620c");
            var animalName = "Rex";

            CreateUser(userId, firstname, lastname);
            var createDog = CreateDog(animalId, userId, animalName);
            var expectedNourishmentLevel = createDog.NourishmentLevel + 1;

            FeedAnimal(animalId);

            var persistedAnimal = _animalApi.GetAnimal(animalId);
            Assert.AreEqual(expectedNourishmentLevel, persistedAnimal.NourishmentLevel);
            Assert.AreEqual(AnimalModel.LifeStatus.Alive, persistedAnimal.Status);
        }

        [Test]
        public void PettingAnAnimalIncreasesMoodLevelBy1()
        {
            var userId = new Guid("2cdf0ec3-ec2b-4511-8b6e-fa89bb2d429e");
            var firstname = "Mark";
            var lastname = "Moller";

            var animalId = new Guid("a7869494-2df0-41f1-b0ec-af7f0425dd63");
            var animalName = "Fenton";

            CreateUser(userId, firstname, lastname);
            var createDog = CreateDog(animalId, userId, animalName);
            var expectedMoodLevel = createDog.MoodLevel + 1;

            PetAnimal(animalId);

            var persistedAnimal = _animalApi.GetAnimal(animalId);
            Assert.AreEqual(expectedMoodLevel, persistedAnimal.MoodLevel);
            Assert.AreEqual(AnimalModel.LifeStatus.Alive, persistedAnimal.Status);
        }

        [Test]
        public void CreatingADogAndPettingButNotFeedingCausesDogToDie()
        {
            var userId = new Guid("5fe25932-db2a-412f-a402-7153295cf1f6");
            var firstname = "Ian";
            var lastname = "Ignova";

            var animalId = new Guid("b74972c2-506e-4d14-9834-85ba455c505e");
            var animalName = "Charlie";
            
            CreateUser(userId, firstname, lastname);
            CreateDog(animalId, userId, animalName);

            PetAnimal(animalId);
            PetAnimal(animalId);

            SetEventExpectation<AnimalKilledV1>(x => x.AnimalId == animalId);
            VerifyEventExpectations();

            var persistedAnimal = _animalApi.GetAnimal(animalId);
            Assert.AreEqual(0, persistedAnimal.MoodLevel);
            Assert.AreEqual(0, persistedAnimal.NourishmentLevel);
            Assert.AreEqual(AnimalModel.LifeStatus.Dead, persistedAnimal.Status);
        }

        [Test]
        public void CreatingADogAndFeedingButNotPettingCausesDogToDie()
        {
            var userId = new Guid("e5391f73-1744-4bba-8386-3972d03acec6");
            var firstname = "Lee";
            var lastname = "Ling";

            var animalId = new Guid("69c5d0d3-863c-463d-80ba-162c45902d70");
            var animalName = "Bobby";

            CreateUser(userId, firstname, lastname);
            CreateDog(animalId, userId, animalName);

            PetAnimal(animalId);
            PetAnimal(animalId);

            SetEventExpectation<AnimalKilledV1>(x => x.AnimalId == animalId);
            VerifyEventExpectations();

            var persistedAnimal = _animalApi.GetAnimal(animalId);
            Assert.AreEqual(0, persistedAnimal.MoodLevel);
            Assert.AreEqual(0, persistedAnimal.NourishmentLevel);
            Assert.AreEqual(AnimalModel.LifeStatus.Dead, persistedAnimal.Status);
        }

        private void FeedAnimal(Guid animalId)
        {
            SetEventExpectation<NourishmentLevelPersistedV1>(x => x.AnimalId == animalId);

            _animalApi.Feed(animalId);

            VerifyEventExpectations();
        }

        private void PetAnimal(Guid animalId)
        {
            SetEventExpectation<MoodLevelPersistedV1>(x => x.AnimalId == animalId);

            _animalApi.Pet(animalId);

            VerifyEventExpectations();
        }
    }
}