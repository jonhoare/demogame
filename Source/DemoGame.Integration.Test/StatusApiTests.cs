﻿using System.Net;
using DemoGame.Integration.Test.ApiWrapper;
using NUnit.Framework;

namespace DemoGame.Integration.Test
{
    [TestFixture]
    public class StatusApiTests
    {
        private StatusApi _statusApi;

        [SetUp]
        public void Init()
        {
            _statusApi = new StatusApi(TestAgent.HttpClient);
        }

        [Test]
        public void ApiStatusReturns200Ok()
        {
            var statusCode = _statusApi.GetStatus();

            Assert.AreEqual(HttpStatusCode.OK, statusCode);
        }
    }
}
