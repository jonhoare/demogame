﻿using System;
using System.Net;
using System.Net.Http;
using DemoGame.Integration.Test.ApiWrapper;
using DemoGame.Integration.Test.Exceptions;
using DemoGame.Integration.Test.Framework;
using DemoGame.User.Contract.V1.Events;
using NUnit.Framework;

namespace DemoGame.Integration.Test
{
    [TestFixture]
    public class UserApiTests : EventTesting
    {
        private UserApi _userApi;

        [SetUp]
        public void Init()
        {
            _userApi = new UserApi(TestAgent.HttpClient);
        }

        private HttpResponseMessage CreateUser(Guid userId, string firstname, string lastname)
        {
            SetEventExpectation<UserCreatedV1>(x => x.UserId == userId);
            var response = _userApi.CreateUser(userId, firstname, lastname);
            VerifyEventExpectations();
            return response;
        }

        [Test]
        public void CreateAUserReturnsAnAcceptedStatusCode()
        {
            var userId = new Guid("ac331944-f6fb-4a4c-8016-623d831f7074");
            var firstname = "James";
            var lastname = "Jenkins";
            
            var response = CreateUser(userId, firstname, lastname);

            Assert.AreEqual(HttpStatusCode.Accepted, response.StatusCode);
        }

        [Test]
        public void GetAKnownUserIdReturnsAUser()
        {
            var userId = new Guid("4d12ddf2-2ca8-4013-b214-79d07a9814fb");
            var firstname = "Tina";
            var lastname = "Twinnings";

            CreateUser(userId, firstname, lastname);

            var user = _userApi.GetUser(userId);

            Assert.AreEqual(userId, user.UserId);
            Assert.AreEqual(firstname, user.Firstname);
            Assert.AreEqual(lastname, user.Lastname);
        }

        [Test]
        public void GetAUserByAnUnknownIdThrowsAnUnexpectedException()
        {
            var userId = new Guid("980ab40a-1015-433c-b5be-584186541ddc");

            try
            {
                _userApi.GetUser(userId);

                Assert.Fail("The code should be throwing an exception right about now!");
            }
            catch (UnexpectedStatusCodeException)
            {
                Assert.Pass();
            }
        }
    }
}