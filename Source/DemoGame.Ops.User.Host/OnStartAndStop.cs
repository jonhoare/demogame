﻿using System;
using System.Configuration;
using System.Threading;
using NServiceBus;
using Serilog;

namespace DemoGame.Ops.User.Host
{
    public class OnStartAndStop : IWantToRunWhenBusStartsAndStops, IDisposable
    {
        public static string MutexName = ConfigurationManager.AppSettings["HostMutexName"];
        private static Mutex _mutex;

        public void Start()
        {
            _mutex = new Mutex(true, MutexName);
            Log.Debug(string.Format("Created mutex {0}.", MutexName));
        }

        public void Stop()
        {
            _mutex.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            if (_mutex != null)
            {
                _mutex.ReleaseMutex();
                _mutex.Dispose();
                _mutex = null;
            }

            Mutex mutex;
            if (!Mutex.TryOpenExisting(MutexName, out mutex))
                return;

            mutex.ReleaseMutex();
            mutex.Dispose();
        }
    }
}