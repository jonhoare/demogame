using NServiceBus;

namespace DemoGame.Ops.User.Host
{
    public class EndpointConfig : IConfigureThisEndpoint
    {
        public void Customize(BusConfiguration configuration)
        {
            DemoGame.User.Endpoint.Framework.EndPointConfig.Configure(configuration);
        }
    }
}
