﻿using System;
using System.Collections.Generic;

namespace DemoGame.User.Domain.Models
{
    public class User
    {
        // Default constructor to allow EntityFramework to load this object.
        private User()
        {
        }

        // Constructor to create a new object with all the properties required.
        public User(Guid userId, string firstname, string lastname)
        {
            UserId = userId;
            Firstname = firstname;
            Lastname = lastname;
        }

        public Guid UserId { get; private set; }
        public string Firstname { get; private set; }
        public string Lastname { get; private set; }
    }
}
