﻿using System;

namespace DemoGame.User.Domain.Repositories
{
    public interface IUserRepository
    {
        Models.User CreateUser(Guid userId, string firstname, string lastname);
        Models.User GetUser(Guid userId);

        void SaveChanges();
    }
}