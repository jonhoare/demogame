﻿using System.Web.Http;
using Serilog;

namespace DemoGame.Endpoint.Api.Controllers
{
    public class StatusController : ApiController
    {
        [HttpGet]
        [Route("status")]
        public IHttpActionResult Status()
        {
            Log.Debug("GET: /status");

            return Ok();
        }
    }
}