﻿using Serilog;
using Serilog.Enrichers;

namespace DemoGame.Endpoint.Api.Framework
{
    public class LoggingConfig
    {
        public static void Configure()
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .Enrich.With<MachineNameEnricher>()
                .Enrich.FromLogContext()
                .CreateLogger();
        }
    }
}