﻿using System.Configuration;
using NServiceBus.Config;
using NServiceBus.Config.ConfigurationSource;

namespace DemoGame.Endpoint.Api.Framework
{
    public class UnicastConfigOverride : IProvideConfiguration<UnicastBusConfig>
    {
        public UnicastBusConfig GetConfiguration()
        {
            return new UnicastBusConfig
            {
                MessageEndpointMappings = new MessageEndpointMappingCollection
                {
                    new MessageEndpointMapping
                    {
                        AssemblyName = "DemoGame.User.Contract",
                        Namespace = "DemoGame.User.Contract.V1.Commands",
                        Endpoint = ConfigurationManager.AppSettings["UserEndpointName"]
                    },
                    new MessageEndpointMapping
                    {
                        AssemblyName = "DemoGame.Animal.Contract",
                        Namespace = "DemoGame.Animal.Contract.V1.Commands",
                        Endpoint = ConfigurationManager.AppSettings["AnimalEndpointName"]
                    },
                    new MessageEndpointMapping
                    {
                        AssemblyName = "DemoGame.User.Contract",
                        Namespace = "DemoGame.User.Contract.V1.Events",
                        Endpoint = ConfigurationManager.AppSettings["UserEndpointName"]
                    },
                    new MessageEndpointMapping
                    {
                        AssemblyName = "DemoGame.Animal.Contract",
                        Namespace = "DemoGame.Animal.Contract.V1.Events",
                        Endpoint = ConfigurationManager.AppSettings["AnimalEndpointName"]
                    }
                }
            };
        }
    }
}