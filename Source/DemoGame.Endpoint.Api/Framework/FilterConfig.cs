﻿using System.Web.Http;

namespace DemoGame.Endpoint.Api.Framework
{
    public class FilterConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            config.Filters.Add(new LogErrorFilterAttribute());
        }
    }
}