﻿using System.Web.Http;
using DemoGame.Endpoint.Api.Framework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace DemoGame.Endpoint.Api.Framework
{
    public class Startup
    {
        public virtual void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            LoggingConfig.Configure();
            FilterConfig.Configure(config);
            DependencyConfig.Configure(config);
            EndpointConfig.ConfigureAndStartBus();

            app.UseAutofacMiddleware(DependencyConfig.Container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);

        }
    }
}