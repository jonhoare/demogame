﻿using System.Web.Http.Filters;
using Serilog;

namespace DemoGame.Endpoint.Api.Framework
{
    public class LogErrorFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            Log.Error(context.Exception, "DemoGame Api Error");
        }
    }
}