﻿using System;
using System.Configuration;
using NServiceBus;
using NServiceBus.Logging;
using NServiceBus.Serilog;

namespace DemoGame.Endpoint.Api.Framework
{
    public class EndpointConfig
    {
        public static string EndpointName = ConfigurationManager.AppSettings["EndpointName"];

        public static void ConfigureAndStartBus()
        {
            LogManager.Use<SerilogFactory>();

            var configuration = new BusConfiguration();

            configuration.LicensePath(string.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, "License.xml"));
            configuration.EndpointName(EndpointName);
            configuration.ScaleOut().UseSingleBrokerQueue();
            configuration.UseContainer<AutofacBuilder>(c => c.ExistingLifetimeScope(DependencyConfig.Container));
            configuration.EnableInstallers();
            configuration.UseTransport<AzureServiceBusTransport>().ConnectionStringName("AzureServiceBus");
            configuration.UseSerialization<JsonSerializer>();
            configuration.UsePersistence<InMemoryPersistence>();
            configuration.Conventions()
                .DefiningCommandsAs(
                    c => c.Namespace == "DemoGame.Animal.Contract.V1.Commands" ||
                         c.Namespace == "DemoGame.User.Contract.V1.Commands"
                )
                .DefiningEventsAs(
                    c => c.Namespace == "DemoGame.Animal.Contract.V1.Events"
                      || c.Namespace == "DemoGame.User.Contract.V1.Events"
                );

            Bus.Create(configuration).Start();
        }
    }
}