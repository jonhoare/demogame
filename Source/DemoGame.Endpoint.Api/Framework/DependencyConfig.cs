﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;

namespace DemoGame.Endpoint.Api.Framework
{
    public class DependencyConfig
    {
        public static IContainer Container { get; private set; }

        public static void Configure(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<User.Api.Framework.AutofacModule>();
            builder.RegisterModule<Animal.Api.Framework.AutofacModule>();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            Container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
        }
    }
}