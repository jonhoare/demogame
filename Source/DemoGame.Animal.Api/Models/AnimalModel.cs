﻿using System;

namespace DemoGame.Animal.Api.Models
{
    public class AnimalModel
    {
        public Guid AnimalId { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int NourishmentLevel { get; set; }
        public int MoodLevel { get; set; }
        public Domain.Models.Animal.LifeStatus Status { get; set; }

        public static AnimalModel LoadFromDto(Domain.Models.Animal animal)
        {
            return new AnimalModel
            {
                AnimalId = animal.AnimalId,
                UserId = animal.UserId,
                Name = animal.Name,
                Type = animal.GetType().Name,
                NourishmentLevel = animal.NourishmentLevel,
                MoodLevel = animal.MoodLevel,
                Status = animal.Status
            };
        }
    }
}