﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using Module = Autofac.Module;

namespace DemoGame.Animal.Api.Framework
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterModule<Persistence.AutofacModule>();
            builder.RegisterModule<Application.AutofacModule>();
        }
    }
}