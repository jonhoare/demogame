﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using DemoGame.Animal.Api.Models;
using DemoGame.Animal.Application;
using DemoGame.Animal.Contract.V1.Commands;
using NServiceBus;
using Serilog;

namespace DemoGame.Animal.Api.Controllers
{
    [RoutePrefix("v1/animal")]
    public class AnimalController : ApiController
    {
        private readonly IBus _bus;
        private readonly IAnimalService _animalService;

        public AnimalController(IBus bus, IAnimalService animalService)
        {
            _animalService = animalService;
            _bus = bus;
        }

        [HttpPut]
        [Route("dog/{animalId:guid}")]
        public IHttpActionResult PutCreateDog(Guid animalId, AnimalModel model)
        {
            Log.Debug("PUT: /v1/animal/dog/{0}", animalId);

            _bus.Send(new CreateDogV1(animalId, model.UserId, model.Name));

            ScaffoldAnimalCreatedReturnModel(animalId, "Dog", model);

            return Content(HttpStatusCode.Accepted, model);
        }

        [HttpPut]
        [Route("cat/{animalId:guid}")]
        public IHttpActionResult PutCreateCat(Guid animalId, AnimalModel model)
        {
            Log.Debug("PUT: /v1/animal/cat/{0}", animalId);

            _bus.Send(new CreateCatV1(animalId, model.UserId, model.Name));

            ScaffoldAnimalCreatedReturnModel(animalId, "Cat", model);

            return Content(HttpStatusCode.Accepted, model);
        }

        [HttpPut]
        [Route("rabbit/{animalId:guid}")]
        public IHttpActionResult PutCreateRabbit(Guid animalId, AnimalModel model)
        {
            Log.Debug("PUT: /v1/animal/rabbit/{0}", animalId);

            _bus.Send(new CreateRabbitV1(animalId, model.UserId, model.Name));

            ScaffoldAnimalCreatedReturnModel(animalId, "Rabbit", model);

            return Content(HttpStatusCode.Accepted, model);
        }

        [HttpPut]
        [Route("feed/{animalId:guid}")]
        public IHttpActionResult PutFeed(Guid animalId)
        {
            Log.Debug("PUT: /v1/animal/feed/{0}", animalId);

            _bus.Send(new FeedAnimalV1(animalId));

            return StatusCode(HttpStatusCode.Accepted);
        }

        [HttpPut]
        [Route("pet/{animalId:guid}")]
        public IHttpActionResult PutPet(Guid animalId)
        {
            Log.Debug("PUT: /v1/animal/pet/{0}", animalId);

            _bus.Send(new PetAnimalV1(animalId));

            return StatusCode(HttpStatusCode.Accepted);
        }

        [HttpGet]
        [Route("{animalId:guid}")]
        public IHttpActionResult GetAnimal(Guid animalId)
        {
            Log.Debug("GET: /v1/animal/{0}", animalId);

            var animalDto = _animalService.GetAnimal(animalId);

            if (animalDto == null)
                return NotFound();

            return Ok(AnimalModel.LoadFromDto(animalDto));
        }

        [HttpGet]
        [Route("user/{userId:guid}")]
        public IHttpActionResult GetAnimals(Guid userId)
        {
            Log.Debug("GET: /v1/animal/user/{0}", userId);

            var animalDtos = _animalService.GetAnimals(userId);

            return Ok(animalDtos.Select(AnimalModel.LoadFromDto));
        }

        private static void ScaffoldAnimalCreatedReturnModel(Guid animalId, string animalType, AnimalModel model)
        {
            model.AnimalId = animalId;
            model.Type = animalType;
            model.NourishmentLevel = Domain.Models.Animal.StartingNourishmentLevel;
            model.MoodLevel = Domain.Models.Animal.StartingMoodLevel;
        }
    }
}