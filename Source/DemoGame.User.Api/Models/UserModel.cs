﻿using System;

namespace DemoGame.User.Api.Models
{
    public class UserModel
    {
        public Guid UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public static UserModel LoadFromDto(User.Domain.Models.User user)
        {
            return new UserModel
            {
                UserId = user.UserId,
                Firstname = user.Firstname,
                Lastname = user.Lastname
            };
        }
    }
}