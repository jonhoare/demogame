﻿using System;
using System.Net;
using System.Web.Http;
using DemoGame.User.Api.Models;
using DemoGame.User.Application;
using DemoGame.User.Contract.V1.Commands;
using NServiceBus;
using Serilog;

namespace DemoGame.User.Api.Controllers
{
    [RoutePrefix("v1/user")]
    public class UserController : ApiController
    {
        private readonly IBus _bus;
        private readonly IUserService _userService;

        public UserController(IBus bus, IUserService userService)
        {
            _userService = userService;
            _bus = bus;
        }

        [HttpGet]
        [Route("{userId:guid}")]
        public IHttpActionResult GetUser(Guid userId)
        {
            Log.Debug("GET: /v1/user/{0}", userId);

            var userDto = _userService.GetUser(userId);

            if (userDto == null)
                return NotFound();

            return Ok(UserModel.LoadFromDto(userDto));
        }

        [HttpPut]
        [Route("{userId:guid}")]
        public IHttpActionResult PutUser(Guid userId, UserModel model)
        {
            Log.Debug("PUT: /v1/user/{0}", userId);

            _bus.Send(new CreateUserV1(userId, model.Firstname, model.Lastname));

            model.UserId = userId;
            return Content(HttpStatusCode.Accepted, model);
        }
    }
}