﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class PersistMoodLevelV1
    {
        private PersistMoodLevelV1()
        {
        }

        public PersistMoodLevelV1(Guid animalId, int moodLevel)
        {
            AnimalId = animalId;
            MoodLevel = moodLevel;
        }

        public Guid AnimalId { get; set; }
        public int MoodLevel { get; set; }
    }
}