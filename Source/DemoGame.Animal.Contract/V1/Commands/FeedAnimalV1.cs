﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class FeedAnimalV1
    {
        private FeedAnimalV1()
        {
        }

        public FeedAnimalV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}