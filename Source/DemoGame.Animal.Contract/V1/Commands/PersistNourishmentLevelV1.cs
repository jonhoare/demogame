﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class PersistNourishmentLevelV1
    {
        private PersistNourishmentLevelV1()
        {
        }

        public PersistNourishmentLevelV1(Guid animalId, int nourishmentLevel)
        {
            AnimalId = animalId;
            NourishmentLevel = nourishmentLevel;
        }

        public Guid AnimalId { get; set; }
        public int NourishmentLevel { get; set; }
    }
}