﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class IncreaseHungerV1
    {
        private IncreaseHungerV1()
        {
        }

        public IncreaseHungerV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}