﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class CreateDogV1
    {
        private CreateDogV1()
        {
        }

        public CreateDogV1(Guid animalId, Guid userId, string name)
        {
            UserId = userId;
            AnimalId = animalId;
            Name = name;
        }

        public Guid UserId { get; set; }
        public Guid AnimalId { get; set; }
        public string Name { get; set; }
    }
}