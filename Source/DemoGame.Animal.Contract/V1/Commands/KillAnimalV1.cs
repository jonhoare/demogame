﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class KillAnimalV1
    {
        private KillAnimalV1()
        {
        }

        public KillAnimalV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}