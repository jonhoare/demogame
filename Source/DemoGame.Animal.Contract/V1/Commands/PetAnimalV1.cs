﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class PetAnimalV1
    {
        private PetAnimalV1()
        {
        }

        public PetAnimalV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}