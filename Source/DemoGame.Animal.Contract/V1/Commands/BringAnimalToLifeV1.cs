﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class BringAnimalToLifeV1
    {
        private BringAnimalToLifeV1()
        {
        }

        public BringAnimalToLifeV1(Guid animalId,
            TimeSpan nourishmentBurnRate, 
            TimeSpan moodBurnRate, 
            int maxMoodLevel, 
            int maxNourishmentLevel)
        {
            AnimalId = animalId;
            NourishmentBurnRate = nourishmentBurnRate;
            MoodBurnRate = moodBurnRate;
            MaxMoodLevel = maxMoodLevel;
            MaxNourishmentLevel = maxNourishmentLevel;
        }
        
        public Guid AnimalId { get; set; }
        public TimeSpan NourishmentBurnRate { get; set; }
        public TimeSpan MoodBurnRate { get; set; }
        public int MaxMoodLevel { get; set; }
        public int MaxNourishmentLevel { get; set; }
    }
}