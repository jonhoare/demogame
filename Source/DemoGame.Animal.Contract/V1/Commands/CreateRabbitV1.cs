﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class CreateRabbitV1
    {
        private CreateRabbitV1()
        {
        }

        public CreateRabbitV1(Guid animalId, Guid userId, string name)
        {
            UserId = userId;
            AnimalId = animalId;
            Name = name;
        }

        public Guid UserId { get; set; }
        public Guid AnimalId { get; set; }
        public string Name { get; set; }
    }
}