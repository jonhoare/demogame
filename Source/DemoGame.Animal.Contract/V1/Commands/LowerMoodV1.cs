﻿using System;

namespace DemoGame.Animal.Contract.V1.Commands
{
    public class LowerMoodV1
    {
        private LowerMoodV1()
        {
        }

        public LowerMoodV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}