﻿using System;

namespace DemoGame.Animal.Contract.V1.Events
{
    public class RabbitCreatedV1
    {
        private RabbitCreatedV1()
        {
        }

        public RabbitCreatedV1(Guid userId, Guid animalId)
        {
            UserId = userId;
            AnimalId = animalId;
        }

        public Guid UserId { get; set; }
        public Guid AnimalId { get; set; }
    }
}