﻿
using System;

namespace DemoGame.Animal.Contract.V1.Events
{
    public class NourishmentLevelPersistedV1
    {
        private NourishmentLevelPersistedV1()
        {
        }

        public NourishmentLevelPersistedV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}