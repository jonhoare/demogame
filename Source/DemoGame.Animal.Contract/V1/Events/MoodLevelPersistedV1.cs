﻿using System;

namespace DemoGame.Animal.Contract.V1.Events
{
    public class MoodLevelPersistedV1
    {
        private MoodLevelPersistedV1()
        {
        }

        public MoodLevelPersistedV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}