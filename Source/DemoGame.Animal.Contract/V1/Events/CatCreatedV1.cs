﻿using System;

namespace DemoGame.Animal.Contract.V1.Events
{
    public class CatCreatedV1
    {
        private CatCreatedV1()
        {
        }

        public CatCreatedV1(Guid userId, Guid animalId)
        {
            UserId = userId;
            AnimalId = animalId;
        }

        public Guid UserId { get; set; }
        public Guid AnimalId { get; set; }
    }
}