﻿using System;

namespace DemoGame.Animal.Contract.V1.Events
{
    public class AnimalKilledV1
    {
        private AnimalKilledV1()
        {
        }

        public AnimalKilledV1(Guid animalId)
        {
            AnimalId = animalId;
        }

        public Guid AnimalId { get; set; }
    }
}