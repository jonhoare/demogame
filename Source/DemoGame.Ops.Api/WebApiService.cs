﻿using DemoGame.Endpoint.Api.Framework;
using DemoGame.Ops.Api.Properties;
using Serilog;

namespace DemoGame.Ops.Api
{
    public class WebApiService
    {
        public void Start()
        {
            Microsoft.Owin.Hosting.WebApp.Start<Startup>(Settings.Default.BaseAddress);

            Log.Information("WebApi is running...");
        }

        public void Stop()
        {
        }
    }
}