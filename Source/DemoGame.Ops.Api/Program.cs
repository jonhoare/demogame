﻿using DemoGame.Endpoint.Api.Framework;
using Topshelf;

namespace DemoGame.Ops.Api
{
    class Program
    {
        static void Main(string[] args)
        {
            LoggingConfig.Configure();

            HostFactory.Run(host =>
            {
                host.UseSerilog();
                host.Service<WebApiService>(service =>
                {
                    service.ConstructUsing(worker => new WebApiService());
                    service.WhenStarted(worker => worker.Start());
                    service.WhenStopped(worker => worker.Stop());
                });
                host.RunAsLocalSystem();

                host.SetDescription("DemoGame Api");
                host.SetDisplayName("DemoGame Api");
                host.SetServiceName("DemoGame.Ops.Api");
            });
        }
    }
}
